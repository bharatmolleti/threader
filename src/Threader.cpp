#include "ThreadManager.h"
#include <iostream>

using namespace std;

int main (int argc, char *argv[])
{
	cout<<"Main Starts"<<endl;

	ThreadManager* manager = ThreadManager::getInstance();

	manager->start(4, 10);
	manager->stop();

	cout<<"Main Ends"<<endl;
}
