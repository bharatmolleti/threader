/*
 * ThreadManager.h
 *
 *  Created on: Sep 18, 2014
 *      Author: bharat
 */

#ifndef THREADMANAGER_H_
#define THREADMANAGER_H_

#include "pthread.h"

const int T_ERROR = -1;
const int T_OK = 0;
const int T_EOF = 1;
const int T_MEMORY = -2;
const int T_INVALID = -3;

class ThreadManager
{
private:
	ThreadManager();
	static ThreadManager pManager;
	static void* _threadProcess(void* args);
	void* mData;
public:
	virtual ~ThreadManager();
	static ThreadManager* getInstance() { return &pManager;}
	void start(int threadCount,int tasksCount);
	void stop();
};


#endif /* THREADMANAGER_H_ */
