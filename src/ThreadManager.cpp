/*
 * ThreadManager.cpp
 *
 *  Created on: Sep 18, 2014
 *      Author: bharat
 */

#include "ThreadManager.h"
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

ThreadManager ThreadManager::pManager;

class Task {
public:
	Task(int totalTasks);
	int updateTask(int tid); // increments the task and fills up the taskHolder
	~Task();
private:
	int start();
	int stop();
	pthread_mutex_t mLock;
	int mTotalTasks;
	int mCurrentIndex;
	long* mTaskHolder; // which of the threads is holding this task?
	boolean mInitPassed;
};

Task::Task(int totalTasks) {
	mTotalTasks = totalTasks;
	mCurrentIndex = 0;
	mTaskHolder = NULL;
	mInitPassed = false;
	start();
}

Task::~Task() {
	stop();
}

int Task::start() {
	stop();
	mTaskHolder = (long*) malloc(sizeof(long) * mTotalTasks);
	if (mTaskHolder == NULL) {
		return T_MEMORY;
	}
	pthread_mutex_init(&mLock, NULL);
	mInitPassed = true;
	return T_OK;
}

int Task::stop() {
	if (mTaskHolder) {
		printf("\n Progress Report");
		for (int i = 0; i < mTotalTasks; i++) {
			printf("\n Task: %d was done by Thread : %ld ", i, mTaskHolder[i]);
		}
		pthread_mutex_destroy(&mLock);
		free(mTaskHolder);
		mTaskHolder = NULL;
	}
	mCurrentIndex = 0;
	mInitPassed = false;
	return T_OK;
}

int Task::updateTask(int tid) {
	if (mInitPassed == false) {
		printf("\n ERROR! Insufficient Memory.. Reinitialize Object!");
		return T_ERROR;
	}
	printf("\nWaiting for Lock: TID : %d", tid);
	pthread_mutex_lock(&mLock);
	printf("\nObtained Lock : TID : %d", tid);
	if (mCurrentIndex >= mTotalTasks) {
		printf("\nReleasing Lock: TID: %d ,  All the tasks are complete :)",
				tid);
		pthread_mutex_unlock(&mLock);
		return T_EOF;
	}
	printf("\nTask Holder is %p, currentIndex is %d : TID: %d ", mTaskHolder,
			mCurrentIndex, tid);
	mTaskHolder[mCurrentIndex] = tid;
	printf("\nReleasing Lock : task:( %d/ %d), TID : %d", mCurrentIndex,
			mTotalTasks, tid);
	mCurrentIndex++;
	pthread_mutex_unlock(&mLock);
	return T_OK;
}

class TData {
public:
	TData(int threadCount, int taskCount, void* (*)(void*));
	virtual ~TData();
	int start();
	int stop();
private:
	pthread_attr_t mAttr;

	int mThreadCount;
	int mTaskCount;

	pthread_t* mThread;
	Task* mTask;

	void* (*mFunction)(void*);
};

TData::TData(int threadCount, int taskCount, void* (*fn)(void*)) {
	mThreadCount = threadCount;
	mTaskCount = taskCount;

	mThread = NULL;
	mTask = NULL;

	mFunction = fn;

	pthread_attr_init(&mAttr);
	pthread_attr_setdetachstate(&mAttr, PTHREAD_CREATE_JOINABLE);
}

TData::~TData() {
	stop();
	pthread_attr_destroy(&mAttr);
}

int TData::stop() {
	if (mThread) {
		void *status;
		for (int t = 0; t < mThreadCount; t++) {
			printf("\n Waiting for thread %d to join ", t);
			int rc = pthread_join(mThread[t], &status);
			if (rc) {
				printf("\nERROR; return code from pthread_join() is %d", rc);
			}
			printf("\n Finished join with thread %d having a status of %ld", t,
					(long) status);
		}
		free(mThread);
		mThread = NULL;
	}
	if (mTask) {
		delete mTask;
		mTask = NULL;
	}
	return T_OK;
}

int TData::start() {
	stop();
	mThread = (pthread_t*) malloc(sizeof(pthread_t) * mThreadCount);
	mTask = new Task(mTaskCount);

	if (mThread == NULL || mTask == NULL || mFunction == NULL
			|| mThreadCount <= 0 || mTaskCount <= 0) {
		return T_INVALID;
	}

	for (int t = 0; t < mThreadCount; t++) {
		printf("\nCreating thread %d (DS : %p) (FN : %p)", t, mTask, mFunction);
		if (pthread_create(&mThread[t], &mAttr, mFunction, (void *) mTask)) {
			printf("\nERROR in pthread_create()");
			return T_ERROR;
		}
	}
	return T_OK;
}


ThreadManager::ThreadManager():mData(NULL) {
}

ThreadManager::~ThreadManager() {
	stop();
}

void ThreadManager::start(int instancesCount, int tasksCount) {
	printf("\n ThreadManager::start + (%p) ", _threadProcess);
	TData* tData = new TData(instancesCount, tasksCount, _threadProcess);
	tData->start();
	mData = tData;
	printf("\nThreadManager::start -");
}

void ThreadManager::stop() {
	printf("\n ThreadManager::stop() +");
	TData* tData = (TData*) mData;
	if (tData) {
		tData->stop();
	}
	printf("\n ThreadManager::stop() !");
}

void* ThreadManager::_threadProcess(void* t) {
	static int counter = 0;
	counter++;
	int tid = counter;
	int i;
	double result = 0.0;
	Task* task = (Task*) t;
	printf("\n My ID is %d (%p)", tid, task);
	while (task->updateTask(tid) == T_OK) {
		printf("\nStarting Thread %d ...", tid);
		for (i = 0; i < 1000000; i++) {
			result = result + sin(i) * tan(i);
		}
		printf("\nThread %d done. Result = %e", tid, result);
	}
	pthread_exit((void*) t);
	printf("\n The total calls to this function are: %d", counter);

	return NULL;
}
